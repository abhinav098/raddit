Rails.application.routes.draw do
  devise_for :users
resources :comments
  resources :links do
    resources :comments
    member do
      put "like",   to:"links#upvote"
      put "dislike",to:"links#downvote"
    end
  end

  resources :comments
  root to: "links#index"

end
